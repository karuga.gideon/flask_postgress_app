from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://gidemn:77Admin2018#@localhost/python_apps'
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200), unique=True)
    email = db.Column(db.String(250), unique=True)
    password = db.Column(db.String(250))
    intrash = db.Column(db.String(20))

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password

    def __repr__(self):
        return '<User %r>' % self.username

db.create_all()         

@app.route('/')
def index():
    users = User.query.all()
    for user in users:
        print(user.email)
    # return jsonify(users)
    return "Hello Posgres. Printed user emails..."


if __name__ == "__main__":
    # app.run(debug=True, host='0.0.0.0', port=87)
    app.run(debug=True)
